<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProsesController extends Controller
{
    function create(){
        return view('create021190064');
    }

    public function proses(Request $request){
        $data = array();
        $data['nama'] =$request->nama;
        $data['npm'] =$request->npm;
        $data['programstudi'] =$request->programstudi;
        $data['nohp'] =$request->nohp;
        $data['tempattanggallahir'] =$request->tempattanggallahir;
        $data['jeniskelamin'] =$request->jeniskelamin;
        $data['agama'] =$request->agama;


        return view('view021190064', ['data' => $data]);
    }
}